public class App {
    public static void main(String[] args) throws Exception {
        /*
         * Comment nhiều dòng (khối)
         * ví dụ sử dụng biên string
         */
        System.out.println("Hello, World!");
        String strName = new String("Devcamp");
        System.out.println(strName);//comment 1 dòng: in biên strName ra console
        /**
         * Ví dụ sử dụng các phương thức của lớp String
         */
        System.out.println("Chuyen ve chu thuong: ToLowerCase: " + strName.toLowerCase());
        System.out.println("Chuyen ve chu hoa: ToUpperCase: " + strName.toUpperCase());
        System.out.println("Chieu dai cua chuoi: length: " + strName.length());
    }
 }
 
